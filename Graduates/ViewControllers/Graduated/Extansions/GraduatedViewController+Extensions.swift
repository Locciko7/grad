//
//  GraduatedViewController+Extensions.swift
//  Graduates
//
//  Created by Loci Olah on 27.10.2021.
//

import Foundation
import UIKit

extension GraduatedViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return haveGraduated.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: studentCellID, for: indexPath) as! StudentsTableViewCell
        return cell
    }
    
}
