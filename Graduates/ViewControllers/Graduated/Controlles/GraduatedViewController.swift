//
//  GraduatedViewController.swift
//  Graduates
//
//  Created by Loci Olah on 27.10.2021.
//

import Foundation
import UIKit

class GraduatedViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var haveGraduated: [StudentModel] = []
    let studentCellID = "StudentCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    tableView.register(UINib(nibName: studentCellID, bundle: nil), forCellReuseIdentifier: studentCellID)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }

}
