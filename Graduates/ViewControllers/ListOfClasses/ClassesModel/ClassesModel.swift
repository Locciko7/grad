//
//  ClassesModel.swift
//  Graduates
//
//  Created by Loci Olah on 27.10.2021.
//

import Foundation

struct Group {
    
    var className: String
    var studentsList: [StudentModel]
    
}
