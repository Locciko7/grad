//
//  ListOfClassesViewController+Extensions.swift
//  Graduates
//
//  Created by Loci Olah on 29.10.2021.
//

import Foundation
import UIKit

extension ListOfClasses: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupsArrat.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellID, for: indexPath) as! GroupeTableViewCell
        cell.update(text: groupsArrat[indexPath.row].className)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let studentVC = (self.tabBarController?.viewControllers?[1] as? ListStudentsViewController)
        studentVC?.students = self.groupsArrat[indexPath.row].studentsList
        self.tabBarController?.selectedIndex = 1
    }
}
