//
//  GroupeTableViewCell.swift
//  Graduates
//
//  Created by Loci Olah on 29.10.2021.
//

import UIKit

class GroupeTableViewCell: UITableViewCell {

   
    @IBOutlet weak var groupeRow: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func update(text: String) {
        groupeRow.text = text
    }
}
