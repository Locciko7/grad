//
//  ListOfClasses.swift
//  Graduates
//
//  Created by Loci Olah on 27.10.2021.
//

import Foundation
import UIKit

class ListOfClasses: UIViewController {

    @IBOutlet weak var listOfClassesTableview: UITableView!
    
    let tableViewCellID = "ClassTableViewCell"
    var groupsArrat: [Group] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        groupsArrat = SourceManager().setupClasses()
        
        listOfClassesTableview.register(UINib(nibName: tableViewCellID, bundle: nil), forCellReuseIdentifier: tableViewCellID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        listOfClassesTableview.reloadData()
    }
}
