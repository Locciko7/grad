//
//  ModelListStudents.swift
//  Graduates
//
//  Created by Loci Olah on 27.10.2021.
//

import Foundation

struct StudentModel {
    var id: String
    var firstName: String
    var secondName: String
    var isSelected: Bool
    
    var groupe: String
}
