//
//  ListStudentsViewController.swift
//  Graduates
//
//  Created by Loci Olah on 27.10.2021.
//

import Foundation
import UIKit

class ListStudentsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let studentCellID = "studentCell"
    var students: [StudentModel] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: studentCellID, bundle: nil), forCellReuseIdentifier: studentCellID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if students.isEmpty {
            students = SourceManager().setUpStudents()
        }
        tableView.reloadData()
    }
}
