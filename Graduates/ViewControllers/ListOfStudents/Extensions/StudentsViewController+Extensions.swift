//
//  StudentsViewController+Extensions.swift
//  Graduates
//
//  Created by Loci Olah on 27.10.2021.
//

import UIKit

extension ListStudentsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: studentCellID, for: indexPath) as! StudentsTableViewCell
        cell.update(student: students[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            var graduatedStudents: [StudentModel] = []
            
            let deletedVC = (self.tabBarController?.viewControllers?[3] as? GraduatedViewController)
            graduatedStudents = deletedVC?.haveGraduated ?? []
            graduatedStudents.append(students[indexPath.row])
            
            students.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
            deletedVC?.haveGraduated = graduatedStudents

        }
        
    }
}


