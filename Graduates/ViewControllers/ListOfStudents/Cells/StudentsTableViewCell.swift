//
//  StudentsTableViewCell.swift
//  Graduates
//
//  Created by Loci Olah on 27.10.2021.
//

import UIKit

class StudentsTableViewCell: UITableViewCell {

    @IBOutlet weak var firstNameStudentLabel: UILabel!
    @IBOutlet weak var secondNameStudentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func update(student: StudentModel) {
        firstNameStudentLabel.text = student.firstName
        secondNameStudentLabel.text = student.secondName
    }
    
}
