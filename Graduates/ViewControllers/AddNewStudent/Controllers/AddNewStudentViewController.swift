//
//  AddNewStudentViewController.swift
//  Graduates
//
//  Created by Loci Olah on 27.10.2021.
//

import Foundation
import UIKit

class AddNewStudent: UIViewController {
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var secondNameTextField: UITextField!
    
    var students: [StudentModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
  
    @IBAction func addNew(_ sender: Any) {
        addStudent()
    }
   
    func addStudent() {
        guard let firstName = firstNameTextField.text, let secondName = secondNameTextField.text, !firstName.isEmpty, !secondName.isEmpty else {
            return
        }
        
        let addedStudent = StudentModel(id: "10", firstName: firstName, secondName: secondName, isSelected: false, groupe: "1")
        
        let studentsVC = (self.tabBarController?.viewControllers?[1] as? ListStudentsViewController)
        self.students = studentsVC?.students ?? []
        self.students = SourceManager().addStudent(student: addedStudent, students: students)
        studentsVC?.students = self.students
        self.tabBarController?.selectedIndex = 1
    }
    
    func showErrorAlert() {
        let alertVC = UIAlertController(title: "Error", message: "Please, enter all fields!", preferredStyle: .alert)
        let  cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertVC.addAction(cancelAction)
        present(alertVC, animated: true, completion: nil)
    }
}
