//
//  SourceManager.swift
//  Graduates
//
//  Created by Loci Olah on 27.10.2021.
//

import Foundation

class SourceManager {
    
    func getStudent() -> [StudentModel] {
        return setUpStudents()
    }
    
    func deleteStudent(id: String, students: [StudentModel]) -> [StudentModel] {
        return students.filter({$0.id != id})
    }
    
    func addStudent(student: StudentModel, students: [StudentModel]) -> [StudentModel] {
        var tempStudent = students
        tempStudent.append(student)
        return tempStudent.sorted(by: {$0.firstName < $1.firstName})
    }
    
    func setUpStudents() -> [StudentModel] {
        
        let firstStudent = StudentModel(id: "1", firstName: "Pop", secondName: "Petya", isSelected: false, groupe: "11")
        let secondStudent = StudentModel(id: "1", firstName: "Pop", secondName: "Petya", isSelected: false, groupe: "11")
        let thirdStudent = StudentModel(id: "1", firstName: "Pop", secondName: "Petya", isSelected: false, groupe: "11")
        let foursStudent = StudentModel(id: "1", firstName: "Pop", secondName: "Petya", isSelected: false, groupe: "11")
        let fifthStudent = StudentModel(id: "1", firstName: "Pop", secondName: "Petya", isSelected: false, groupe: "11")
        let sixStudent = StudentModel(id: "1", firstName: "Pop", secondName: "Petya", isSelected: false, groupe: "11")
        let sevenStudent = StudentModel(id: "1", firstName: "Pop", secondName: "Petya", isSelected: false, groupe: "11")
        
        return [firstStudent, secondStudent, thirdStudent, foursStudent, fifthStudent, sixStudent, sevenStudent]
    }
    
    func setupClasses() -> [Group]{
        
        let group1 = Group(className: "11-A", studentsList: setUpStudents())
        let group2 = Group(className: "11-B", studentsList: setUpStudents())
        let group3 = Group(className: "9-A", studentsList: setUpStudents())
        let group4 = Group(className: "9-B", studentsList: setUpStudents())
        
        
        return [group1, group2, group3, group4]
    }
}
